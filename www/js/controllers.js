angular.module('starter.controllers', ['firebase'])

  .controller('CurrentCtrl', function ($scope, $ionicLoading, $timeout) {

    var ctx = document.getElementById("myChart_current");

    function getData() {

      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });


      firebase.database().ref("Sensors").on('value', function (data) {
        $scope.chart_data = {"labels": [], "datasets": []};

        $scope.sensor_data = data.val();
        var sensors = data.val();       //Local variable to improve code readability
        var index = 0;

        for (var sensor in sensors) {
          var timestamps = [];
          $scope.chart_data.datasets.push({"data": [], "label": ""});

          for (var reading in sensors[sensor]) {
            if (timestamps[index] == undefined || timestamps == null || timestamps.length <= 0) {
              var date = moment(new Date(sensors[sensor][reading].Timestamp)).format('L') + " " + moment(new Date(sensors[sensor][reading].Timestamp)).format('hh:mm');

              timestamps.push(date);
            }
            $scope.chart_data.datasets[index].data.push(sensors[sensor][reading].Current);
          }

          $scope.chart_data.datasets[index].label = sensors[sensor].Name;
          $scope.chart_data.datasets[index]['tension'] = 0;

          $scope.chart_data.datasets[index]['backgroundColor'] = getRandomColorAlpha();
          $scope.chart_data.labels = timestamps;

          // $scope.chart_data.labels[index].push(sensors[sensor][])
          index++;
        }

        var myChart = new Chart(ctx,
          {
            "type": "line",
            "data": $scope.chart_data,
            "options": {
              "scales": {
                "xAxes": [{
                  "ticks": {
                    "autoSkip": false,
                    "maxRotation": 45
                  }
                }],
                "yAxes": [{
                  "scaleLabel": {
                    "display": true,
                    "labelString": "Current/A"
                  }
                }]
              }
            }
          });
        $ionicLoading.hide();

      });
    }

    getData();


    function getRandomColorAlpha() {
      var r = Math.random() * 254;
      var g = Math.random() * 254;
      var b = Math.random() * 254;


      // console.log(r);

      return 'rgba(' + parseInt(r) + ',' + parseInt(g) + ',' + parseInt(b) + ',0.2)';

    }

    //
    // var myChart = new Chart(ctx, {
    //     type: 'line',
    //     data: {
    //         labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Violet"],
    //         datasets: [{
    //             tension: 0,
    //             label: '# of Votes',
    //             data: [12, 19, 3, 5, 2, 3, 10],
    //             backgroundColor: [
    //                 'rgba(255, 99, 0, 0.2)',
    //                 'rgba(54, 162, 0, 0.2)',
    //                 'rgba(255, 206, 86, 0.2)',
    //                 'rgba(75, 192, 192, 0.2)',
    //                 'rgba(153, 102, 255, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             borderColor: [
    //                 'rgba(255,99,132,1)',
    //                 'rgba(54, 162, 235, 1)',
    //                 'rgba(255, 206, 86, 1)',
    //                 'rgba(75, 192, 192, 1)',
    //                 'rgba(153, 102, 255, 1)',
    //                 'rgba(255, 159, 64, 1)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             borderWidth: 1
    //         }]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero: true
    //                 }
    //             }]
    //         }
    //     }
    // });
    //

  })

  .controller('VoltageCtrl', function ($scope, $ionicLoading) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    var ctx = document.getElementById("myChart_voltage");

    function getData() {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });

      firebase.database().ref("Sensors").on('value', function (data) {
        $scope.chart_data = {"labels": [], "datasets": []};
        $scope.sensor_data = data.val();
        var sensors = data.val();       //Local variable to improve code readability
        var index = 0;

        for (var sensor in sensors) {
          var timestamps = [];
          $scope.chart_data.datasets.push({"data": [], "label": ""});

          for (var reading in sensors[sensor]) {
            if (timestamps[index] == undefined || timestamps == null || timestamps.length <= 0) {
              var date = moment(new Date(sensors[sensor][reading].Timestamp)).format('L') + " " + moment(new Date(sensors[sensor][reading].Timestamp)).format('hh:mm');
              timestamps.push(date);
            }
            $scope.chart_data.datasets[index].data.push(sensors[sensor][reading].Voltage);
          }

          $scope.chart_data.datasets[index].label = sensors[sensor].Name;
          $scope.chart_data.datasets[index]['tension'] = 0;

          $scope.chart_data.datasets[index]['backgroundColor'] = getRandomColorAlpha();
          $scope.chart_data.labels = timestamps;

          // $scope.chart_data.labels[index].push(sensors[sensor][])
          index++;
        }

        var myChart = new Chart(ctx,
          {
            "type": "line",
            "data": $scope.chart_data,
            "options": {
              "scales": {
                "xAxes": [{
                  "ticks": {
                    "autoSkip": false,
                    "maxRotation": 45
                  }
                }],
                "yAxes": [{
                  "scaleLabel": {
                    "display": true,
                    "labelString": "Voltage/V"
                  }
                }]
              }
            }
          });
        $ionicLoading.hide()

      });
    }

    getData();

    function getRandomColorAlpha() {
      var r = Math.random() * 254;
      var g = Math.random() * 254;
      var b = Math.random() * 254;


      // console.log(r);

      return 'rgba(' + parseInt(r) + ',' + parseInt(g) + ',' + parseInt(b) + ',0.2)';

    }


  })

  .controller('PowerCtrl', function ($scope, $ionicLoading) {


    var ctx = document.getElementById("myChart_power");

    function getData() {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });

      firebase.database().ref("Sensors").on('value', function (data) {
        $scope.chart_data = {"labels": [], "datasets": []};
        $scope.sensor_data = data.val();
        var sensors = data.val();       //Local variable to improve code readability
        var index = 0;

        for (var sensor in sensors) {
          var timestamps = [];
          $scope.chart_data.datasets.push({"data": [], "label": ""});

          for (var reading in sensors[sensor]) {
            if (timestamps[index] == undefined || timestamps == null || timestamps.length <= 0) {
              var date = moment(new Date(sensors[sensor][reading].Timestamp)).format('L') + " " + moment(new Date(sensors[sensor][reading].Timestamp)).format('hh:mm');
              timestamps.push(date);
            }
            $scope.chart_data.datasets[index].data.push(sensors[sensor][reading].Current * sensors[sensor][reading].Voltage);
          }

          $scope.chart_data.datasets[index].label = sensors[sensor].Name;
          $scope.chart_data.datasets[index]['tension'] = 0;

          $scope.chart_data.datasets[index]['backgroundColor'] = getRandomColorAlpha();
          $scope.chart_data.labels = timestamps;

          // $scope.chart_data.labels[index].push(sensors[sensor][])
          index++;
        }

        var myChart = new Chart(ctx,
          {
            "type": "line",
            "data": $scope.chart_data,
            "options": {
              "scales": {
                "xAxes": [{
                  "ticks": {
                    "autoSkip": false,
                    "maxRotation": 45
                  }
                }],
                "yAxes": [{
                  "scaleLabel": {
                    "display": true,
                    "labelString": "Power/W"
                  }
                }]
              }
            }
          });

        $ionicLoading.hide();

      });
    }

    getData();
    function getRandomColorAlpha() {
      var r = Math.random() * 254;
      var g = Math.random() * 254;
      var b = Math.random() * 254;


      // console.log(r);

      return 'rgba(' + parseInt(r) + ',' + parseInt(g) + ',' + parseInt(b) + ',0.2)';

    }

  });
